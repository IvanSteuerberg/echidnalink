(function() {
    // let socket = io.connect(window.location.hostname + ':' + 3000,
    //     {
    //         reconnection: true,
    //         reconnectionDelay: 1000,
    //         reconnectionDelayMax: 5000,
    //         reconnectionAttempts: 99999
    //     });


    let socket = null;

    let show = {
        joystick: false,
        light: false,
        accelerometer: false,
        io: false,
        analogIn: false,
        button: false,
        digitalIn: false
    }

    function showInput(type) {
        for (let key in show) {
            if (key == type) {
                show[key] = true;
            } else {
                show[key] = false;
            }
        }
    }

    function connect() {
        if (socket) {
            socket.destroy();
            delete socket;
            socket = null;
        }
        socket = io.connect(window.location.hostname + ':' + 3000);
        socket.on('connect', function() {
            console.log('connected to server, ID: ' + socket.id);
        });
        socket.on('disconnect', data => {
            console.log('disconnected from server - ' + data);

        });
    }

    function disconnect() {
        socket.emit('client_disconnect', { id: socket.id });
        socket.disconnect(true);
        socket.destroy();
        delete socket;
        socket = null;
    }


    connect();

    let buttonConectar = document.getElementById('connect');
    let buttonDesConectar = document.getElementById('disconnect');
    let buttonSocketStatus = document.getElementById('socket_status');
    let buttonReset = document.getElementById('reset');
    let buttonRojoOn = document.getElementById('turnon_rojo');
    let buttonRojoOff = document.getElementById('turnoff_rojo');
    let buttonYellowOn = document.getElementById('turnon_yellow');
    let buttonYellowOff = document.getElementById('turnoff_yellow');
    let buttonGreenOn = document.getElementById('turnon_green');
    let buttonGreenOff = document.getElementById('turnoff_green');
    let buttonRGBOn = document.getElementById('turnon_rgb');
    let inputRed = document.getElementById('red');
    let inputGreen = document.getElementById('green');
    let inputBlue = document.getElementById('blue');
    let inputFrequency = document.getElementById('frequency');
    let inputDuration = document.getElementById('duration');
    let buttonPlayFrequency = document.getElementById('play_frequency');
    let buttonStopBuzzer = document.getElementById('stop_buzzer');
    let buttonPlaySong = document.getElementById('play_song');
    let buttonReadJoystick = document.getElementById('read_joystick');
    let buttonReadAccelerometer = document.getElementById('read_accelerometer');
    let buttonReadLDR = document.getElementById('read_ldr');
    let buttonReadD2 = document.getElementById('read_d2');
    let buttonReadD3 = document.getElementById('read_d3');
    let buttonReadD4 = document.getElementById('read_d4');
    let buttonReadD7 = document.getElementById('read_d7');
    let buttonReadD8 = document.getElementById('read_d8');
    let buttonReadA0 = document.getElementById('read_a0');
    let buttonReadA1 = document.getElementById('read_a1');
    let buttonReadA2 = document.getElementById('read_a2');
    let buttonReadA3 = document.getElementById('read_a3');
    let buttonReadA4 = document.getElementById('read_a4');
    let buttonReadA5 = document.getElementById('read_a5');
    let buttonReadA6 = document.getElementById('read_a6');
    let buttonReadA7 = document.getElementById('read_a7');
    let buttonWriteDO2_ON = document.getElementById('write_do2_on');
    let buttonWriteDO2_OFF = document.getElementById('write_do2_on');
    let buttonWriteDO3_ON = document.getElementById('write_do3_on');
    let buttonWriteDO3_OFF = document.getElementById('write_do3_off');
    let buttonWriteDO4_ON = document.getElementById('write_do4_on');
    let buttonWriteDO4_OFF = document.getElementById('write_do4_off');
    let buttonWriteDO5_ON = document.getElementById('write_do5_on');
    let buttonWriteDO5_OFF = document.getElementById('write_do5_off');
    let buttonWriteDO6_ON = document.getElementById('write_do6_on');
    let buttonWriteDO6_OFF = document.getElementById('write_do6_off');
    let buttonWriteDO7_ON = document.getElementById('write_do7_on');
    let buttonWriteDO7_OFF = document.getElementById('write_do7_off');
    let buttonWriteDO8_ON = document.getElementById('write_do8_on');
    let buttonWriteDO8_OFF = document.getElementById('write_do8_off');
    let buttonWriteDO9_ON = document.getElementById('write_do9_on');
    let buttonWriteDO9_OFF = document.getElementById('write_do9_off');
    let buttonWriteDO11_ON = document.getElementById('write_do11_on');
    let buttonWriteDO11_OFF = document.getElementById('write_do11_off');
    let buttonWriteDO12_ON = document.getElementById('write_do12_on');
    let buttonWriteDO12_OFF = document.getElementById('write_do12_off');
    let buttonWriteDO13_ON = document.getElementById('write_do13_on');
    let buttonWriteDO13_OFF = document.getElementById('write_do13_off');
    let buttonReadAnalogIn = document.getElementById('read_analog_in');
    let buttonReadButton = document.getElementById('read_buttons');
    let inputServoPin = document.getElementById('servo_pin');
    let inputServoAngle = document.getElementById('servo_angle');
    let buttonServoRun = document.getElementById('servo_run');

    let eventList = document.getElementById('event_list');

    buttonConectar.addEventListener('click', () => {
        connect();
    });

    buttonDesConectar.addEventListener('click', () => {
        disconnect();
    });

    buttonSocketStatus.addEventListener('click', () => {
        console.log(socket);
    });

    buttonReset.addEventListener('click', () => {
        socket.emit('reset', {});
    })

    // Echidna outputs
    buttonRojoOn.addEventListener('click', () => {
        socket.emit('led', { name: 'red', action: 'on' });
    });
    buttonRojoOff.addEventListener('click', () => {
        socket.emit('led', { name: 'red', action: 'off' });
    });

    buttonYellowOn.addEventListener('click', () => {
        socket.emit('led', { name: 'yellow', action: 'on' });
    });
    buttonYellowOff.addEventListener('click', () => {
        socket.emit('led', { name: 'yellow', action: 'off' });
    });

    buttonGreenOn.addEventListener('click', () => {
        socket.emit('led', { name: 'green', action: 'on' });
    });
    buttonGreenOff.addEventListener('click', () => {
        socket.emit('led', { name: 'green', action: 'off' });
    });


    buttonWriteDO2_ON.addEventListener('click', () => {
        socket.emit('digitalOutput', { id: 'd2', value: 1 });
    });
    buttonWriteDO2_OFF.addEventListener('click', () => {
        socket.emit('digitalOutput', { id: 'd2', value: 0 });
    });

    buttonWriteDO3_ON.addEventListener('click', () => {
        socket.emit('digitalOutput', { id: 'd3', value: 1 });
    });
    buttonWriteDO3_OFF.addEventListener('click', () => {
        socket.emit('digitalOutput', { id: 'd3', value: 0 });
    });

    buttonWriteDO4_ON.addEventListener('click', () => {
        socket.emit('digitalOutput', { id: 'd4', value: 1 });
    });
    buttonWriteDO4_OFF.addEventListener('click', () => {
        socket.emit('digitalOutput', { id: 'd4', value: 0 });
    });

    buttonWriteDO5_ON.addEventListener('click', () => {
        socket.emit('digitalOutput', { id: 'd5', value: 1 });
    });
    buttonWriteDO5_OFF.addEventListener('click', () => {
        socket.emit('digitalOutput', { id: 'd5', value: 0 });
    });

    buttonWriteDO6_ON.addEventListener('click', () => {
        socket.emit('digitalOutput', { id: 'd6', value: 1 });
    });
    buttonWriteDO6_OFF.addEventListener('click', () => {
        socket.emit('digitalOutput', { id: 'd6', value: 0 });
    });

    buttonWriteDO7_ON.addEventListener('click', () => {
        socket.emit('digitalOutput', { id: 'd7', value: 1 });
    });
    buttonWriteDO7_OFF.addEventListener('click', () => {
        socket.emit('digitalOutput', { id: 'd7', value: 0 });
    });

    buttonWriteDO8_ON.addEventListener('click', () => {
        socket.emit('digitalOutput', { id: 'd8', value: 1 });
    });
    buttonWriteDO8_OFF.addEventListener('click', () => {
        socket.emit('digitalOutput', { id: 'd8', value: 0 });
    });

    buttonWriteDO9_ON.addEventListener('click', () => {
        socket.emit('digitalOutput', { id: 'd9', value: 1 });
    });
    buttonWriteDO9_OFF.addEventListener('click', () => {
        socket.emit('digitalOutput', { id: 'd9', value: 0 });
    });

    buttonWriteDO11_ON.addEventListener('click', () => {
        socket.emit('digitalOutput', { id: 'd11', value: 1 });
    });
    buttonWriteDO11_OFF.addEventListener('click', () => {
        socket.emit('digitalOutput', { id: 'd11', value: 0 });
    });

    buttonWriteDO12_ON.addEventListener('click', () => {
        socket.emit('digitalOutput', { id: 'd12', value: 1 });
    });
    buttonWriteDO12_OFF.addEventListener('click', () => {
        socket.emit('digitalOutput', { id: 'd12', value: 0 });
    });

    buttonWriteDO13_ON.addEventListener('click', () => {
        socket.emit('digitalOutput', { id: 'd13', value: 1 });
    });
    buttonWriteDO13_OFF.addEventListener('click', () => {
        socket.emit('digitalOutput', { id: 'd13', value: 0 });
    });


    buttonRGBOn.addEventListener('click', () => {
        let c = {
            red: parseInt(inputRed.value),
            green: parseInt(inputGreen.value),
            blue: parseInt(inputBlue.value)
        };
        console.log(c);
        socket.emit('rgb', { color: c, action: 'color' });
    });

    buttonPlayFrequency.addEventListener('click', () => {
        socket.emit('piezo', {
            action: 'frequency',
            frequency: parseInt(inputFrequency.value),
            duration: parseInt(inputDuration.value)
        })
    });

    buttonServoRun.addEventListener('click', () => {
        socket.emit('servo', {
            pin: inputServoPin.value,
            angle: inputServoAngle.value
        })
    })

    buttonPlaySong.addEventListener('click', () => {

        socket.emit('piezo', {
            action: 'play',
            tune: mario_fanfare,
            callback: null
        });
    });

    buttonStopBuzzer.addEventListener('click', () => {
        socket.emit('piezo', { action: 'off' })
    });

    buttonReadJoystick.addEventListener('click', () => {
        showInput('joystick');
    })

    buttonReadAccelerometer.addEventListener('click', () => {
        showInput('accelerometer');
    })

    buttonReadLDR.addEventListener('click', () => {
        showInput('light');
    })

    buttonReadD2.addEventListener('click', () => {

        socket.emit('digitalInput', { id: 'd2' });

        // let p = new Promise((resolve, reject) => {
        //     socket.emit('digitalInput', { id: 'd2' });
        //     socket.on('digitalInputValue', (message) => {
        //         resolve(message);
        //     })
        // })

        // p.then(message => {
        //     console.log(message);
        //     showInput('digitalIn');
        //     let li = document.createElement('li');
        //     li.innerHTML = 'D2: ' + message.value;
        //     eventList.insertBefore(li, eventList.firstChild);

        // })

    })

    buttonReadD3.addEventListener('click', () => {

        let p = new Promise((resolve, reject) => {
            socket.emit('digitalInput', { id: 'd3' });
            socket.on('digitalInputValue', (value) => {
                resolve(value);
            })
        })

        p.then(message => {
            console.log(message);
            showInput('digitalIn');
            let li = document.createElement('li');
            li.innerHTML = 'D3: ' + message.value;
            eventList.insertBefore(li, eventList.firstChild);

        })

    })

    buttonReadD4.addEventListener('click', () => {

        let p = new Promise((resolve, reject) => {
            socket.emit('digitalInput', { id: 'd4' });
            socket.on('digitalInputValue', (value) => {
                resolve(value);
            })
        })

        p.then(message => {
            console.log(message);
            showInput('digitalIn');
            let li = document.createElement('li');
            li.innerHTML = 'D4: ' + message.value;
            eventList.insertBefore(li, eventList.firstChild);

        })

    })

    buttonReadD7.addEventListener('click', () => {

        let p = new Promise((resolve, reject) => {
            socket.emit('digitalInput', { id: 'd7' });
            socket.on('digitalInputValue', (value) => {
                resolve(value);
            })
        })

        p.then(message => {
            console.log(message);
            showInput('digitalIn');
            let li = document.createElement('li');
            li.innerHTML = 'D7: ' + message.value;
            eventList.insertBefore(li, eventList.firstChild);

        })

    })

    buttonReadD8.addEventListener('click', () => {

        let p = new Promise((resolve, reject) => {
            socket.emit('digitalInput', { id: 'd8' });
            socket.on('digitalInputValue', (value) => {
                resolve(value);
            })
        })

        p.then(message => {
            console.log(message);
            showInput('digitalIn');
            let li = document.createElement('li');
            li.innerHTML = 'D8: ' + message.value;
            eventList.insertBefore(li, eventList.firstChild);

        })

    })

    buttonReadA0.addEventListener('click', () => {

        let p = new Promise((resolve, reject) => {
            socket.emit('analogInput', { id: 'a0' });
            socket.on('analogInputValue', (value) => {
                resolve(value);
            })
        })

        p.then(message => {
            console.log(message);
            showInput('analogIn');
            let li = document.createElement('li');
            li.innerHTML = 'A0: ' + message.value;
            eventList.insertBefore(li, eventList.firstChild);

        })
    })

    buttonReadA1.addEventListener('click', () => {

        let p = new Promise((resolve, reject) => {
            socket.emit('analogInput', { id: 'a1' });
            socket.on('analogInputValue', (value) => {
                resolve(value);
            })
        })

        p.then(message => {
            console.log(message);
            showInput('analogIn');
            let li = document.createElement('li');
            li.innerHTML = 'A1: ' + message.value;
            eventList.insertBefore(li, eventList.firstChild);

        })
    })
    buttonReadA2.addEventListener('click', () => {

        let p = new Promise((resolve, reject) => {
            socket.emit('analogInput', { id: 'a2' });
            socket.on('analogInputValue', (value) => {
                resolve(value);
            })
        })

        p.then(message => {
            console.log(message);
            showInput('analogIn');
            let li = document.createElement('li');
            li.innerHTML = 'A2: ' + message.value;
            eventList.insertBefore(li, eventList.firstChild);

        })
    })
    buttonReadA3.addEventListener('click', () => {

        let p = new Promise((resolve, reject) => {
            socket.emit('analogInput', { id: 'a3' });
            socket.on('analogInputValue', (value) => {
                resolve(value);
            })
        })

        p.then(message => {
            console.log(message);
            showInput('analogIn');
            let li = document.createElement('li');
            li.innerHTML = 'A3: ' + message.value;
            eventList.insertBefore(li, eventList.firstChild);

        })
    })
    buttonReadA4.addEventListener('click', () => {

        let p = new Promise((resolve, reject) => {
            socket.emit('analogInput', { id: 'a4' });
            socket.on('analogInputValue', (value) => {
                resolve(value);
            })
        })

        p.then(message => {
            console.log(message);
            showInput('analogIn');
            let li = document.createElement('li');
            li.innerHTML = 'A4: ' + message.value;
            eventList.insertBefore(li, eventList.firstChild);

        })
    })
    buttonReadA5.addEventListener('click', () => {

        let p = new Promise((resolve, reject) => {
            socket.emit('analogInput', { id: 'a5' });
            socket.on('analogInputValue', (value) => {
                resolve(value);
            })
        })

        p.then(message => {
            console.log(message);
            showInput('analogIn');
            let li = document.createElement('li');
            li.innerHTML = 'A5: ' + message.value;
            eventList.insertBefore(li, eventList.firstChild);

        })
    })

    buttonReadA6.addEventListener('click', () => {

        let p = new Promise((resolve, reject) => {
            socket.emit('analogInput', { id: 'a6' });
            socket.on('analogInputValue', (value) => {
                resolve(value);
            })
        })

        p.then(message => {
            console.log(message);
            showInput('analogIn');
            let li = document.createElement('li');
            li.innerHTML = 'A6: ' + message.value;
            eventList.insertBefore(li, eventList.firstChild);

        })
    })

    buttonReadA7.addEventListener('click', () => {

        let p = new Promise((resolve, reject) => {
            socket.emit('analogInput', { id: 'a7' });
            socket.on('analogInputValue', (value) => {
                resolve(value);
            })
        })

        p.then(message => {
            console.log(message);
            showInput('analogIn');
            let li = document.createElement('li');
            li.innerHTML = 'A7: ' + message.value;
            eventList.insertBefore(li, eventList.firstChild);

        })
    })


    buttonReadAnalogIn.addEventListener('click', () => {
        showInput('analogIn');
    })

    buttonReadButton.addEventListener('click', () => {
        showInput('button');
    })

    buttonReset.addEventListener('click', () => {
        socket.emit('reset');
    })

    // Echidna inputs
    // socket.on('button', data => {
    //     let li = document.createElement('li');
    //     li.innerHTML = data.name + ' ' + data.action;
    //     eventList.insertBefore(li, eventList.firstChild);
    // });


    socket.on('joystick', data => {
        if (!show.joystick) return;
        if (data.x < 0.1 && data.y < 0.1) return;
        let li = document.createElement('li');
        li.innerHTML = 'JOYSTICK => jx = ' + data.x + ', jy = ' + data.y;
        eventList.insertBefore(li, eventList.firstChild);
    });

    socket.on('light', data => {
        if (!show.light) return;
        let li = document.createElement('li');
        li.innerHTML = 'LIGHT => level = ' + data.level;
        eventList.insertBefore(li, eventList.firstChild);
    });

    socket.on('accelerometer', data => {
        if (!show.accelerometer) return;

        let li = document.createElement('li');
        li.innerHTML = 'ACCELEROMETER => ax = ' + data.x + ', ay = ' + data.y + ' az = ' + data.z;
        eventList.insertBefore(li, eventList.firstChild);
    })

    socket.on('io', data => {
        if (!show.io) return;
        let li = document.createElement('li');
        li.innerHTML = 'IO => id = ' + data.id + ' value = ' + data.status.value;
        eventList.insertBefore(li, eventList.firstChild);
    })

    socket.on('analog_in', data => {
        console.log('Analog IN 1');
        if (!show.analogIn) return;
        console.log('Analog IN');
        let li = document.createElement('li');
        li.innerHTML = 'Analog In => value = ' + data.value;
        eventList.insertBefore(li, eventList.firstChild);
    })


    socket.on('button_read', data => {
        console.log('client: button status event received on response to a query');
        console.log(data);
        let li = document.createElement('li');
        li.innerHTML = 'Buton id:' + data.id + '  value = ' + data.value;
        eventList.insertBefore(li, eventList.firstChild);
    })

    socket.on('digitalInputValue', data => {
        console.log("digitalInputValue");
        console.log(data);
    })

    socket.on('connect', () => {
        socket.emit('join', 'Client is connected!');
    });
}());

const mario_intro = {
    song: [
        ["E5", 1 / 4],
        [null, 1 / 4],
        ["E5", 1 / 4],
        [null, 3 / 4],
        ["E5", 1 / 4],
        [null, 3 / 4],
        ["C5", 1 / 4],
        [null, 1 / 4],
        ["E5", 1 / 4],
        [null, 3 / 4],
        ["G5", 1 / 4],
        [null, 7 / 4],
        ["G4", 1 / 4],
        [null, 7 / 4]
    ],
    tempo: 200
};;

const tetris = {
    song: [
        ["E5", 1],
        [null, 1 / 4],
        ["B4", 1],
        ["C5", 1],
        ["D5", 1],
        [null, 1 / 4],
        ["C5", 1],
        ["B4", 1],

        ["A4", 1],
        [null, 1 / 4],
        ["A4", 1],
        ["C5", 1],
        ["E5", 1],
        [null, 1 / 4],
        ["D5", 1],
        ["C5", 1],

        ["B4", 1],
        [null, 1 / 4],
        [null, 1 / 4],
        ["C5", 1],
        ["D5", 1],
        [null, 1 / 4],
        ["E5", 1],
        [null, 1 / 4],

        ["C5", 1],
        [null, 1 / 4],
        ["A4", 1],
        [null, 1 / 4],
        ["A4", 1],
        [null, 1 / 4],
        [null, 1 / 4],
        [null, 1 / 4],

        ["D5", 1],
        [null, 1 / 4],
        [null, 1 / 4],
        ["F5", 1],
        ["A5", 1],
        [null, 1 / 4],
        ["G5", 1],
        ["F5", 1],

        ["E5", 1],
        [null, 1 / 4],
        [null, 1 / 4],
        ["C5", 1],
        ["E5", 1],
        [null, 1 / 4],
        ["D5", 1],
        ["C5", 1],

        ["B4", 1],
        [null, 1 / 4],
        ["B4", 1],
        ["C5", 1],
        ["D5", 1],
        [null, 1 / 4],
        ["E5", 1],
        [null, 1 / 4],

        ["C5", 1],
        [null, 1 / 4],
        ["A4", 1],
        [null, 1 / 4],
        ["A4", 1],
        [null, 1 / 4],
        [null, 1 / 4],
        [null, 1 / 4],

        ["E5", 1],
        [null, 1 / 4],
        ["B4", 1],
        ["C5", 1],
        ["D5", 1],
        [null, 1 / 4],
        ["C5", 1],
        ["B4", 1],

        ["A4", 1],
        [null, 1 / 4],
        ["A4", 1],
        ["C5", 1],
        ["E5", 1],
        [null, 1 / 4],
        ["D5", 1],
        ["C5", 1],

        ["B4", 1],
        [null, 1 / 4],
        [null, 1 / 4],
        ["C5", 1],
        ["D5", 1],
        [null, 1 / 4],
        ["E5", 1],
        [null, 1 / 4],

        ["C5", 1],
        [null, 1 / 4],
        ["A4", 1],
        [null, 1 / 4],
        ["A4", 1],
        [null, 1 / 4],
        [null, 1 / 4],
        [null, 1 / 4],

        ["E5", 1],
        [null, 1 / 4],
        ["B4", 1],
        ["C5", 1],
        ["D5", 1],
        [null, 1 / 4],
        ["C5", 1],
        ["B4", 1],

        ["A4", 1],
        [null, 1 / 4],
        ["A4", 1],
        ["C5", 1],
        ["E5", 1],
        [null, 1 / 4],
        ["D5", 1],
        ["C5", 1],

        ["B4", 1],
        [null, 1 / 4],
        [null, 1 / 4],
        ["C5", 1],
        ["D5", 1],
        [null, 1 / 4],
        ["E5", 1],
        [null, 1 / 4],

        ["C5", 1],
        [null, 1 / 4],
        ["A4", 1],
        [null, 1 / 4],
        ["A4", 1],
        [null, 1 / 4],
        [null, 1 / 4],
        [null, 1 / 4],

        ["D5", 1],
        [null, 1 / 4],
        [null, 1 / 4],
        ["F5", 1],
        ["A5", 1],
        [null, 1 / 4],
        ["G5", 1],
        ["F5", 1],

        ["E5", 1],
        [null, 1 / 4],
        [null, 1 / 4],
        ["C5", 1],
        ["E5", 1],
        [null, 1 / 4],
        ["D5", 1],
        ["C5", 1],

        ["B4", 1],
        [null, 1 / 4],
        ["B4", 1],
        ["C5", 1],
        ["D5", 1],
        [null, 1 / 4],
        ["E5", 1],
        [null, 1 / 4],

        ["C5", 1],
        [null, 1 / 4],
        ["A4", 1],
        [null, 1 / 4],
        ["A4", 1],
        [null, 1 / 4],
        [null, 1 / 4],
        [null, 1 / 4],

        ["E4", 1],
        [null, 1 / 4],
        [null, 1 / 4],
        [null, 1 / 4],
        ["C4", 1],
        [null, 1 / 4],
        [null, 1 / 4],
        [null, 1 / 4],

        ["D4", 1],
        [null, 1 / 4],
        [null, 1 / 4],
        [null, 1 / 4],
        ["B3", 1],
        [null, 1 / 4],
        [null, 1 / 4],
        [null, 1 / 4],

        ["C4", 1],
        [null, 1 / 4],
        [null, 1 / 4],
        [null, 1 / 4],
        ["A3", 1],
        [null, 1 / 4],
        [null, 1 / 4],
        [null, 1 / 4],

        ["G#3", 1],
        [null, 1 / 4],
        [null, 1 / 4],
        [null, 1 / 4],
        ["B3", 1],
        [null, 1 / 4],
        [null, 1 / 4],
        [null, 1 / 4],

        ["D4", 1],
        [null, 1 / 4],
        [null, 1 / 4],
        [null, 1 / 4],
        ["B3", 1],
        [null, 1 / 4],
        [null, 1 / 4],
        [null, 1 / 4],

        ["C4", 1],
        [null, 1 / 4],
        ["E4", 1],
        [null, 1 / 4],
        ["A4", 1],
        [null, 1 / 4],
        [null, 1 / 4],
        [null, 1 / 4],

        ["G#4", 1],
        [null, 1 / 4],
        [null, 1 / 4],
        [null, 1 / 4],
        [null, 1 / 4],
        [null, 1 / 4],
        [null, 1 / 4],
        [null, 1 / 4],

        ["E5", 1],
        [null, 1 / 4],
        ["B4", 1],
        ["C5", 1],
        ["D5", 1],
        [null, 1 / 4],
        ["C5", 1],
        ["B4", 1],

        ["A4", 1],
        [null, 1 / 4],
        ["A4", 1],
        ["C5", 1],
        ["E5", 1],
        [null, 1 / 4],
        ["D5", 1],
        ["C5", 1],

        ["B4", 1],
        [null, 1 / 4],
        [null, 1 / 4],
        ["C5", 1],
        ["D5", 1],
        [null, 1 / 4],
        ["E5", 1],
        [null, 1 / 4],

        ["C5", 1],
        [null, 1 / 4],
        ["A4", 1],
        [null, 1 / 4],
        ["A4", 1],
        [null, 1 / 4],
        [null, 1 / 4],
        [null, 1 / 4],

        ["D5", 1],
        [null, 1 / 4],
        [null, 1 / 4],
        ["F5", 1],
        ["A5", 1],
        [null, 1 / 4],
        ["G5", 1],
        ["F5", 1],

        ["E5", 1],
        [null, 1 / 4],
        [null, 1 / 4],
        ["C5", 1],
        ["E5", 1],
        [null, 1 / 4],
        ["D5", 1],
        ["C5", 1],

        ["B4", 1],
        [null, 1 / 4],
        ["B4", 1],
        ["C5", 1],
        ["D5", 1],
        [null, 1 / 4],
        ["E5", 1],
        [null, 1 / 4],

        ["C5", 1],
        [null, 1 / 4],
        ["A4", 1],
        [null, 1 / 4],
        ["A4", 1],
        [null, 1 / 4],
        [null, 1 / 4],
        [null, 1 / 4],

        ["E5", 1],
        [null, 1 / 4],
        ["B4", 1],
        ["C5", 1],
        ["D5", 1],
        [null, 1 / 4],
        ["C5", 1],
        ["B4", 1],

        ["A4", 1],
        [null, 1 / 4],
        ["A4", 1],
        ["C5", 1],
        ["E5", 1],
        [null, 1 / 4],
        ["D5", 1],
        ["C5", 1],

        ["B4", 1],
        [null, 1 / 4],
        [null, 1 / 4],
        ["C5", 1],
        ["D5", 1],
        [null, 1 / 4],
        ["E5", 1],
        [null, 1 / 4],

        ["C5", 1],
        [null, 1 / 4],
        ["A4", 1],
        [null, 1 / 4],
        ["A4", 1],
        [null, 1 / 4],
        [null, 1 / 4],
        [null, 1 / 4],

        ["D5", 1],
        [null, 1 / 4],
        [null, 1 / 4],
        ["F5", 1],
        ["A5", 1],
        [null, 1 / 4],
        ["G5", 1],
        ["F5", 1],

        ["E5", 1],
        [null, 1 / 4],
        [null, 1 / 4],
        ["C5", 1],
        ["E5", 1],
        [null, 1 / 4],
        ["D5", 1],
        ["C5", 1],

        ["B4", 1],
        [null, 1 / 4],
        ["B4", 1],
        ["C5", 1],
        ["D5", 1],
        [null, 1 / 4],
        ["E5", 1],
        [null, 1 / 4],

        [null, 1]
    ],
    tempo: 250
};

const mario_fanfare = {
    song: [
        ["C5", 1 / 4],
        [null, 5 / 4],
        ["G4", 1 / 4],
        [null, 5 / 4],
        ["E4", 1 / 4],
        [null, 5 / 4],
        ["A4", 1 / 4],
        [null, 1 / 4],
        ["B4", 1 / 4],
        [null, 3 / 4],
        ["A#4", 1 / 4],
        [null, 1 / 4],
        ["A4", 1 / 4],
        [null, 3 / 4],
        ["G4", 1 / 4],
        [null, 1 / 4],
        ["E5", 1 / 4],
        [null, 3 / 4],
        ["G5", 1 / 4],
        [null, 1 / 4],
        ["A5", 1 / 4],
        [null, 3 / 4],
        ["F5", 1 / 4],
        [null, 1 / 4],
        ["G5", 1 / 4],
        [null, 3 / 4],
        ["E5", 1 / 4],
        [null, 3 / 4],
        ["C5", 1 / 4],
        [null, 1 / 4],
        ["D5", 1 / 4],
        [null, 1 / 4],
        ["B4", 1 / 4],
        [null, 3 / 4]
    ],
    tempo: 200
};

const star_wars = {
    song: [
        ["A", 500],
        [null, 50],
        ["A", 500],
        [null, 50],
        ["A", 500],
        [null, 50],
        ["F", 350],
        [null, 50],
        ["C5", 150],
        [null, 50],
        ["A", 500],
        [null, 50],
        ["F", 350],
        [null, 50],
        ["C5", 150],
        [null, 50],
        ["A", 650],
        [null, 50],
        [null, 500],
        ["E5", 500],
        [null, 50],
        ["E5", 500],
        [null, 50],
        ["E5", 500],
        [null, 50],
        ["F5", 350],
        [null, 50],
        ["C5", 150],
        [null, 50],
        ["G4", 500],
        [null, 50],
        ["F", 350],
        [null, 50],
        ["C5", 150],
        [null, 50],
        ["A", 650],
        [null, 50],
        [null, 500],

        ["A5", 500],
        [null, 50],
        ["A", 300],
        [null, 50],
        ["A", 150],
        [null, 50],
        ["A5", 500],
        [null, 50],
        ["G#5", 325],
        [null, 50],
        ["G5", 175],
        [null, 50],
        ["F#5", 125],
        [null, 50],
        ["F5", 125],
        [null, 50],
        ["F#5", 250],
        [null, 50],
        [null, 325],
        ["A4", 250],
        [null, 50],
        ["D#5", 500],
        [null, 50],
        ["D5", 325],
        [null, 50],
        ["C#5", 175],
        [null, 50],
        ["C5", 125],
        [null, 50],
        ["B", 125],
        [null, 50],
        ["C5", 250],
        [null, 50],
        [null, 350],

        ["F", 250],
        [null, 50],
        ["G4", 500],
        [null, 50],
        ["F", 350],
        [null, 50],
        ["A", 125],
        [null, 50],
        ["C5", 500],
        [null, 50],
        ["A", 375],
        [null, 50],
        ["C5", 125],
        [null, 50],
        ["E5", 650],
        [null, 50],
        [null, 500],

        ["A5", 500],
        [null, 50],
        ["A", 300],
        [null, 50],
        ["A", 150],
        [null, 50],
        ["A5", 500],
        [null, 50],
        ["G#5", 325],
        [null, 50],
        ["G5", 175],
        [null, 50],
        ["F#5", 125],
        [null, 50],
        ["F5", 125],
        [null, 50],
        ["F#5", 250],
        [null, 50],
        [null, 325],
        ["A4", 250],
        [null, 50],
        ["D#5", 500],
        [null, 50],
        ["D5", 325],
        [null, 50],
        ["C#5", 175],
        [null, 50],
        ["C5", 125],
        [null, 50],
        ["B", 125],
        [null, 50],
        ["C5", 250],
        [null, 50],
        [null, 350],

        ["F", 250],
        [null, 50],
        ["G4", 500],
        [null, 50],
        ["F", 375],
        [null, 50],
        ["C5", 125],
        [null, 50],
        ["A", 500],
        [null, 50],
        ["F", 375],
        [null, 50],
        ["C5", 125],
        [null, 50],
        ["A", 650],
        [null, 50],
        [null, 650]
    ],
    tempo: 100000
};

const jingle_bells = {
    song: [
        ["E5", 1 / 16],
        [null, 1 / 8],
        ["E5", 1 / 16],
        [null, 1 / 8],
        ["E5", 1 / 16],
        [null, 1 / 4],
        ["E5", 1 / 16],
        [null, 1 / 8],
        ["E5", 1 / 16],
        [null, 1 / 8],
        ["E5", 1 / 16],
        [null, 1 / 4],
        ["E5", 1 / 16],
        [null, 1 / 8],
        ["G5", 1 / 16],
        [null, 1 / 8],
        ["C5", 1 / 8],
        [null, 1 / 8],
        ["D5", 1 / 16],
        [null, 1 / 8],
        ["E5", 1 / 4],
        [null, 2 / 4],
        ["F5", 1 / 16],
        [null, 1 / 8],
        ["F5", 1 / 16],
        [null, 1 / 8],
        ["F5", 1 / 16],
        [null, 1 / 8],
        ["F5", 1 / 16],
        [null, 1 / 8],
        ["F5", 1 / 16],
        [null, 1 / 8],
        ["E5", 1 / 16],
        [null, 1 / 8],
        ["E5", 1 / 16],
        [null, 1 / 8],
        ["E5", 1 / 16],
        [null, 1 / 8],
        ["E5", 1 / 16],
        [null, 1 / 8],
        ["D5", 1 / 16],
        [null, 1 / 8],
        ["D5", 1 / 16],
        [null, 1 / 8],
        ["E5", 1 / 16],
        [null, 1 / 8],
        ["D5", 1 / 16],
        [null, 2 / 8],
        ["G5", 1 / 4],
        [null, 1 / 2],


        ["G5", 1 / 16],
        [null, 1 / 8],
        ["E5", 1 / 16],
        [null, 1 / 8],
        ["D5", 1 / 16],
        [null, 1 / 8],
        ["C5", 1 / 16],
        [null, 1 / 8],
        ["G5", 1 / 8],
        [null, 1 / 4],
        ["G5", 1 / 16],
        [null, 1 / 16],
        ["G5", 1 / 16],
        [null, 1 / 16],
        ["G5", 1 / 16],
        [null, 1 / 8],
        ["E5", 1 / 16],
        [null, 1 / 8],
        ["D5", 1 / 16],
        [null, 1 / 8],
        ["C5", 1 / 16],
        [null, 1 / 8],
        ["A5", 1 / 8],
        [null, 1 / 4],
        ["A5", 1 / 16],
        [null, 1 / 8],
        ["F5", 1 / 16],
        [null, 1 / 8],
        ["E5", 1 / 16],
        [null, 1 / 8],
        ["D5", 1 / 16],
        [null, 1 / 8],
        ["B5", 1 / 8],
        [null, 1 / 4],
        ["A5", 1 / 16],
        [null, 1 / 8],
        ["G5", 1 / 16],
        [null, 1 / 8],
        ["F5", 1 / 8],
        [null, 1 / 8],
        ["D5", 1 / 16],
        [null, 1 / 8],
        ["E5", 1 / 8],
        [null, 1 / 4]
    ],
    tempo: 30
}